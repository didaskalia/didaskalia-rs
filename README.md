# `didaskalia`

[![GitLab repository][repo-badge]][repo-url]
[![MIT licensed][mit-badge]][mit-url]
[![crates.io][crates-io-badge]][crates-io-url]
[![docs.rs][docs-rs-badge]][docs-rs-url]

[repo-badge]: https://img.shields.io/gitlab/last-commit/didaskalia%2Fdidaskalia-rs?gitlab_url=https%3A%2F%2Fgitlab.com&logo=gitlab&link=https%3A%2F%2Fgitlab.com%2Fdidaskalia%2Fdidaskalia-rs
[repo-url]: https://gitlab.com/didaskalia/didaskalia-rs
[mit-badge]: https://img.shields.io/badge/license-MIT-blue.svg
[mit-url]: https://gitlab.com/didaskalia/didaskalia-rs/blob/master/LICENSE
[crates-io-badge]: https://img.shields.io/crates/v/didaskalia.svg
[crates-io-url]: https://crates.io/crates/didaskalia
[docs-rs-badge]: https://img.shields.io/docsrs/didaskalia
[docs-rs-url]: https://docs.rs/didaskalia/latest/didaskalia/

*NOTE: this project is still early-stage - frequent breaking changes expected!*

Didaskalia is an over-complicated language to store text entries and sequences across different languages in a consistent manner. You may use this to store video game dialogue content, or even for UI content.

Consider checking out the [changelog](https://gitlab.com/didaskalia/didaskalia-rs/blob/master/CHANGELOG.md).

## Show me an example!
```dsk
lang english

[hello_world] = Hello World, in English!
[some_paragraph] = ''
This entry contains ...
    ... multiple lines!
''
```
