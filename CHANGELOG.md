# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [unreleased]

## [0.1.2] - 2023-08-26
### Changed
- Visibilty of internal macros in source code.
### Fixed
- Incorrect link attached to crates.io badge.

## [0.1.1] - 2023-08-26
### Added
- Changelog.
- GitLab badge in `README.md`.
- Additional values in `Cargo.toml`.

## [0.1.0] - 2023-08-26
### Added
- Cargo crate `didaskalia`.
- `DocumentBuilder` & `Document` structs
- `Diagnostics` for errors, warnings and such.
- English and French samples.
- API usage example.

[unreleased]: https://gitlab.com/didaskalia/didaskalia-rs/-/compare/v0.1.2...master
[0.1.2]: https://gitlab.com/didaskalia/didaskalia-rs/-/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/didaskalia/didaskalia-rs/-/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/didaskalia/didaskalia-rs/-/tree/v0.1.0
