use std::{cell::RefCell, collections::HashMap, rc::Rc};

use crate::{
    diagnostics::{
        parser::{ContextKind, Error, LintKind},
        with_context, DiagnosticList, Severity,
    },
    entry_collection::{EntryCollection, EntryPath},
    lexer::{
        keyword::Keyword,
        token::{
            Dot, Equal, Identifier, LeftBracket, Newline, RightBracket, Str, StringExpression,
            TokenKind,
        },
        Expectable, Lexer, TokenIter,
    },
    span::Span,
};

pub struct Parser<'a> {
    lexer: Lexer<'a>,

    lang_entries: Option<&'a mut EntryCollection>,
    sections: Vec<(&'a str, bool)>, // (name, empty)

    diagnostics: Rc<RefCell<DiagnosticList<'a>>>,
}

impl<'a> Parser<'a> {
    pub fn new(source: &'a str, diagnostics: Rc<RefCell<DiagnosticList<'a>>>) -> Self {
        Self {
            lexer: Lexer::new(source, diagnostics.clone()),
            lang_entries: None,
            sections: Vec::new(),
            diagnostics,
        }
    }

    fn mark_section_non_empty(&mut self) {
        for (_, is_empty) in self.sections.iter_mut().rev() {
            if *is_empty {
                *is_empty = false;
            } else {
                return;
            }
        }
    }

    fn try_parse_entry_declaration(&mut self) -> bool {
        let entry_start = self.lexer.pos();

        if self.lexer.try_parse::<LeftBracket>().is_none() {
            return false;
        }

        self.mark_section_non_empty();

        let name = with_context!(self.diagnostics; ContextKind::EntryDeclaration => {
            self.lexer.expect::<Identifier>()
        });

        with_context!(self.diagnostics; ContextKind::EntryDeclarationWithName(
            name.as_ref().map(|ident| ident.text).unwrap_or("<unknown>"),
        ) => {
            self.lexer.expect::<RightBracket>();

            self.lexer.consume_empty_lines();
            self.lexer.expect::<Equal>();

            let Some((Some(string), string_span)) = self.expect_string() else {
                return;
            };

            if string.is_empty() {
                self.diagnostics
                    .borrow_mut()
                    .add_lint(LintKind::EmptyEntryValue, string_span);
            } else if string.is_trimmed_empty() {
                self.diagnostics
                    .borrow_mut()
                    .add_lint(LintKind::BlankEntryValue, string_span);
            }

            let Some(name) = name else {
                return;
            };
            let name = name.text;

            // cannot store entry in content since language has not been specified in this file
            // entry is dismissed, error about missing lang directive has already been reported
            let Some(entries) = &mut self.lang_entries else {
                return;
            };

            if !entries.get_section_mut(self.sections.iter().map(|section| section.0)).expect("section was not open before ???").add_entry(name, string) {
                self.diagnostics.borrow_mut().add(
                    Error::EntryAlreadyDeclared,
                    Span::new(entry_start, self.lexer.pos()),
                );
            }
        });

        true
    }

    fn expect_lang_directive(
        &mut self,
        all_entries: &'a mut HashMap<String, EntryCollection>,
    ) -> bool {
        if self.lexer.try_parse_keyword(Keyword::Lang).is_none() {
            self.diagnostics
                .borrow_mut()
                .add(Error::MissingLangDirective, Span::empty(self.lexer.pos()));
            return false;
        }

        let lang = with_context!(self.diagnostics; ContextKind::LangDirective => {
            let lang = self.lexer.expect::<Identifier>()
                .map(|ident| ident.text.to_string());

            self.expect_end_of_statement();

            lang
        });

        let Some(lang) = lang else {
            return false;
        };

        self.lang_entries = Some(all_entries.entry(lang).or_insert_with(EntryCollection::new));

        true
    }

    fn try_parse_lint_directive(&mut self) -> bool {
        #[rustfmt::skip]
        let (keyword, severity) = match () {
            _ if self.lexer.try_parse_keyword(Keyword::Forbid).is_some() => (Keyword::Forbid, Some(Severity::Error)),
            _ if self.lexer.try_parse_keyword(Keyword::Warn).is_some() => (Keyword::Warn, Some(Severity::Warning)),
            _ if self.lexer.try_parse_keyword(Keyword::Inform).is_some() => (Keyword::Inform, Some(Severity::Info)),
            _ if self.lexer.try_parse_keyword(Keyword::Ignore).is_some() => (Keyword::Ignore, None),
            _ => {
                return false;
            }
        };

        with_context!(self.diagnostics; ContextKind::LintDirective(keyword) => {
            let Some(id) = self.lexer.expect::<Identifier>() else {
                return true;
            };

            let is_valid_id = self.diagnostics
                .borrow_mut()
                .override_lint_severity(id.text, severity);
            if !is_valid_id {
                self.diagnostics
                    .borrow_mut()
                    .add(Error::InvalidLintID(id.text), id.span);
            }

            true
        })
    }

    fn try_parse_global_lint_directive(&mut self) -> bool {
        let start_pos = self.lexer.pos();

        if self.lexer.try_parse_keyword(Keyword::Global).is_none() {
            return false;
        }

        #[rustfmt::skip]
        let Some((keyword, severity)) = with_context!(self.diagnostics; ContextKind::GlobalLintDirective => {
            match () {
                _ if self.lexer.try_parse_keyword(Keyword::Forbid).is_some() => Some((Keyword::Forbid, Some(Severity::Error))),
                _ if self.lexer.try_parse_keyword(Keyword::Warn).is_some() => Some((Keyword::Warn, Some(Severity::Warning))),
                _ if self.lexer.try_parse_keyword(Keyword::Inform).is_some() => Some((Keyword::Inform, Some(Severity::Info))),
                _ if self.lexer.try_parse_keyword(Keyword::Ignore).is_some() => Some((Keyword::Ignore, None)),
                _ => {
                    self.lexer.skip_until(TokenKind::Newline);
                    self.diagnostics.borrow_mut().add(
                        Error::InvalidGlobalLintDirective,
                        Span::new(start_pos, self.lexer.pos()),
                    );
                    None
                }
            }
        }) else {
            return true;
        };

        with_context!(self.diagnostics; ContextKind::GlobalLintDirectiveWithKeyword(keyword) => {
            let Some(id) = self.lexer.expect::<Identifier>() else {
                return true;
            };

            let is_valid_id = self.diagnostics
                .borrow_mut()
                .override_lint_severity(id.text, severity);
            if !is_valid_id {
                self.diagnostics
                    .borrow_mut()
                    .add(Error::InvalidLintID(id.text), id.span);
            }

            true
        })
    }

    fn try_parse_misplaced_lang_directive(&mut self) -> bool {
        let start_pos = self.lexer.pos();

        if self.lexer.try_parse_keyword(Keyword::Lang).is_none() {
            return false;
        }

        with_context!(self.diagnostics; ContextKind::MisplacedLangDirective => {
            self.lexer.expect::<Identifier>();
        });

        self.diagnostics.borrow_mut().add(
            Error::MisplacedLangDirective,
            Span::new(start_pos, self.lexer.pos()),
        );

        true
    }

    fn expect_end_of_statement(&mut self) {
        if self.lexer.expect_or_eof::<Newline>().is_none() {
            self.lexer.skip_to_next_line();
        }
        self.lexer.consume_empty_lines()
    }

    fn try_parse_section(&mut self) -> bool {
        let start_pos = self.lexer.pos();

        if self.lexer.try_parse_keyword(Keyword::Section).is_none() {
            return false;
        }

        let Some(id) = with_context!(self.diagnostics; ContextKind::SectionOpening => self.lexer.expect::<Identifier>())
        else {
            return true;
        };

        let section_line_span = Span::new(start_pos, self.lexer.pos());
        let section_name = id.text;

        if let Some(entries) = &mut self.lang_entries {
            entries
                .get_section_mut(self.sections.iter().map(|section| section.0))
                .expect("section was not open before ???")
                .open_section(section_name);
        }

        self.sections.push((section_name, true));
        self.diagnostics.borrow_mut().open_lint_scope();

        self.expect_end_of_statement();

        let found_end_token = with_context!(self.diagnostics; ContextKind::SectionInside(section_name) => {
            while self.lexer.try_parse_keyword(Keyword::End).is_none() {
                if !self.try_parse_statement() {
                    return false;
                }
            }

            true
        });

        self.diagnostics.borrow_mut().close_lint_scope();
        let (_, is_empty_section) = self.sections.pop().expect("failed closing section");

        if !found_end_token {
            with_context!(self.diagnostics; ContextKind::SectionEnding(section_name) => {
                self.diagnostics.borrow_mut().add(Error::SectionLeftUnclosed(section_name), section_line_span);
            })
        }

        if is_empty_section {
            self.diagnostics.borrow_mut().add_lint(
                LintKind::EmptySection,
                Span::new(start_pos, self.lexer.pos()),
            );
        }

        true
    }

    fn try_parse_statement(&mut self) -> bool {
        let mut has_lints = false;
        let lint_start_pos = self.lexer.pos();
        let mut lint_end_pos = self.lexer.pos();
        while self.try_parse_lint_directive() {
            has_lints = true;
            lint_end_pos = self.lexer.pos();
            self.expect_end_of_statement();
        }
        let lint_span = Span::new(lint_start_pos, lint_end_pos);

        self.lexer.consume_empty_lines();

        let Some(token) = self.lexer.peek(0) else {
            if has_lints {
                self.diagnostics
                    .borrow_mut()
                    .add(Error::LintDiretivesMustBeFollowedByStatement, lint_span);
            }
            return false;
        };

        match () {
            _ if self.try_parse_entry_declaration() => {}
            _ if self.try_parse_misplaced_lang_directive() => {}
            _ if self.try_parse_section() => {}
            _ => {
                if has_lints {
                    self.diagnostics.borrow_mut().flush_lint_overrides();
                    self.diagnostics
                        .borrow_mut()
                        .add(Error::LintDiretivesMustBeFollowedByStatement, lint_span);
                    return true;
                }

                let span = token.span();
                self.diagnostics
                    .borrow_mut()
                    .add(Error::UnexpectedToken(token), span);

                self.lexer.skip_to_next_line();

                return true;
            }
        }

        self.diagnostics.borrow_mut().flush_lint_overrides();

        self.expect_end_of_statement();

        true
    }

    pub fn parse(&mut self, all_entries: &'a mut HashMap<String, EntryCollection>) {
        self.diagnostics.borrow_mut().open_lint_scope(); // open file scope

        self.lexer.consume_empty_lines();

        self.expect_lang_directive(all_entries);

        while self.try_parse_global_lint_directive() {
            self.expect_end_of_statement();
        }

        self.diagnostics.borrow_mut().open_lint_scope(); // open global scope

        while self.try_parse_statement() {}

        let mut diagnostics = self.diagnostics.borrow_mut();
        diagnostics.close_lint_scope(); // global scope
        diagnostics.close_lint_scope(); // file scope
        diagnostics.verify_all_lint_scopes_closed();
    }

    fn expect_string(&mut self) -> Option<(Option<StringExpression>, Span)> {
        with_context!(self.diagnostics; ContextKind::ParsingString => {
            let Str { string, span } = self.lexer.consume_string()?;

            let Some(string) = string else {
                return Some((None, span))
            };

            let string_expr_couples = string.0.into_iter().map(|(string, tokens)| {
                let mut tokens = TokenIter::new(&tokens, self.diagnostics.clone());

                let mut section_path = Vec::new();
                let mut entry_name = tokens.expect::<Identifier>()?.text.to_owned();

                while tokens.try_parse::<Dot>().is_some() {
                    let new_entry_name = tokens.expect::<Identifier>()?.text.to_owned();

                    section_path.push(std::mem::replace(&mut entry_name, new_entry_name));
                }

                tokens.expect_eof()?;

                Some((string.into_owned(), EntryPath::new(section_path, entry_name)))
            }).try_fold(Vec::new(), |mut vec, couple| {
                let (string, entry_path) = couple?;

                vec.push((string, entry_path));

                Some(vec)
            })?;

            Some((Some(StringExpression(string_expr_couples, string.1.to_string())), span))
        })
    }
}

#[cfg(test)]
mod tests {
    use std::{cell::RefCell, collections::HashMap, rc::Rc};

    use super::Parser;
    use crate::DiagnosticList;

    use regex::{Captures, Regex};

    fn test_source_validity(source: &str) {
        let mut content = HashMap::new();
        let diagnostics = Rc::new(RefCell::new(DiagnosticList::new()));

        {
            let mut parser = Parser::new(source, diagnostics.clone());
            parser.parse(&mut content);
        }

        let diagnostics = Rc::into_inner(diagnostics).unwrap().into_inner();
        assert!(diagnostics.is_empty());
    }

    fn test_diagnostics(source: &str) {
        let re = Regex::new(r"(<(\d*)\(([\s\S]*?)\)>)").unwrap();

        let mut match_ranges = Vec::new();
        let mut offset = 0;
        let source = re.replace_all(source, |captures: &Captures| {
            let whole_match = captures.get(1).unwrap();
            let whole_range = whole_match.range();

            let repeat_match = captures.get(2).unwrap();
            let repeat_count = repeat_match.as_str().parse::<usize>().unwrap_or(1);

            let inner_match = captures.get(3).unwrap();
            let inner_len = inner_match.range().len();

            let start_pos = whole_range.start - offset;

            for _ in 0..repeat_count {
                match_ranges.push(start_pos..(start_pos + inner_len));
            }

            offset += whole_range.len() - inner_len;

            inner_match.as_str().to_string()
        });

        let mut content = HashMap::new();
        let diagnostics = Rc::new(RefCell::new(DiagnosticList::new()));

        {
            let mut parser = Parser::new(&source, diagnostics.clone());
            parser.parse(&mut content);
        }

        let diagnostics = Rc::into_inner(diagnostics).unwrap().into_inner();

        assert_eq!(
            diagnostics.len(),
            match_ranges.len(),
            "test case does not generate as many expected diagnostics"
        );
        for (i, (diagnostic, range)) in diagnostics.iter().zip(match_ranges).enumerate() {
            assert_eq!(
                diagnostic.span().start.index..diagnostic.span().end.index,
                range,
                "diagnostic #{} does not span the expected range",
                i + 1
            );
        }
    }

    create_validity_tests! {
        test_blank_doc_with_lang_directive "lang language",
        test_blank_doc_with_double_lang_keyword "lang lang",
        test_single_entry_decl
        "
        lang _
        [entry] = value
        ",
        test_double_entry_decl
        "
        lang _
        [a] = A
        [b] = B
        ",
        test_entry_decl_equal_on_newline
        "
        lang _
        [entry]
        = value
        ",
    }

    create_diagnostic_tests! {
        test_entry_decl_missing_identifier
        "
        lang _
        [<(])> = value
        ",
        test_entry_decl_missing_right_bracket
        "
        lang _
        [entry <(=)> value
        ",
        test_entry_decl_missing_identifier_and_right_bracket
        "
        lang _
        [ <2(=)> a
        ",
        test_entry_decl_missing_equal_sign
        "
        lang _
        [entry] <(value)>
        ",
        test_entry_decl_missing_right_bracket_and_equal_sign
        "
        lang _
        [entry <2(value)>
        ",
        test_entry_decl_missing_identifier_and_equal_sign
        "
        lang _
        [ <(])> <(value)>
        ",
        test_entry_decl_missing_identifier_and_right_bracket_and_equal_sign_then_newline
        "
        lang _
        [ value<(\n)><()><()>",
        test_entry_decl_missing_identifier_and_right_bracket_and_equal_sign
        "
        lang _
        [ value<(\n)><()><()>",
        test_entry_decl_warn_empty
        "
        lang _
        [entry] =        <()>",
        test_entry_decl_warn_empty_then_newline
        "
        lang _
        [entry] =        <()>\n",
        test_lang_directive_missing_newline
        "
        lang _ <([)>entry] value
        ",
    }

    macro_rules! create_validity_tests {
        (
            $($test_name:ident $test_source:expr),* $(,)?
        ) => {
            $(
                #[test]
                fn $test_name() {
                    test_source_validity($test_source);
                }
            )*
        };
    }

    macro_rules! create_diagnostic_tests {
        (
            $($test_name:ident $test_source:expr),* $(,)?
        ) => {
            $(
                #[test]
                fn $test_name() {
                    test_diagnostics($test_source);
                }
            )*
        };
    }

    use create_diagnostic_tests;
    use create_validity_tests;
}
