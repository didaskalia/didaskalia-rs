use std::fmt::Display;

use crate::pos::Pos;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Span {
    pub start: Pos,
    pub end: Pos,
}

impl Span {
    pub const ZERO: Self = Self::empty(Pos::ZERO);

    pub const fn new(start: Pos, end: Pos) -> Self {
        Self { start, end }
    }

    pub const fn empty(pos: Pos) -> Self {
        Self::new(pos, pos)
    }

    pub const fn at(pos: Pos, c: char) -> Self {
        Self::new(pos, pos.advance(c))
    }

    pub fn slice<'a>(&self, source: &'a str) -> &'a str {
        &source[self.start.index..self.end.index]
    }

    pub fn is_empty(&self) -> bool {
        self.start.index >= self.end.index
    }
}

impl Display for Span {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}->{}", self.start, self.end)
    }
}
