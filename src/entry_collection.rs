use std::collections::HashMap;

use crate::lexer::token::StringExpression;

#[derive(Default)]
pub struct EntryCollection {
    entries: HashMap<String, StringExpression>,
    sections: HashMap<String, EntryCollection>,
}

#[derive(Debug)]
pub struct EntryPath {
    section_path: Vec<String>,
    entry_name: String,
}

impl EntryCollection {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn add_entry(&mut self, entry_name: &str, value: StringExpression) -> bool {
        if self.contains_entry(entry_name) {
            return false;
        }

        self.entries.insert(entry_name.to_owned(), value);
        true
    }

    pub fn open_section(&mut self, section_name: &str) {
        if self.contains_section(section_name) {
            return;
        }

        self.sections
            .insert(section_name.to_owned(), EntryCollection::new());
    }

    fn contains_entry(&self, entry_name: &str) -> bool {
        self.entries.contains_key(entry_name)
    }

    fn contains_section(&self, section_name: &str) -> bool {
        self.sections.contains_key(section_name)
    }

    pub fn get_entry_with_path(&self, entry_path: &EntryPath) -> Option<&StringExpression> {
        self.get_section(
            entry_path
                .section_path
                .iter()
                .map(|section| section.as_str()),
        )?
        .get_entry(&entry_path.entry_name)
    }

    pub fn get_entry(&self, entry_name: &str) -> Option<&StringExpression> {
        self.entries.get(entry_name)
    }

    pub fn get_section<'a>(&self, section_path: impl Iterator<Item = &'a str>) -> Option<&Self> {
        let mut section = self;

        for section_name in section_path {
            section = section.sections.get(section_name)?;
        }

        Some(section)
    }

    pub fn get_section_mut<'a>(
        &mut self,
        section_path: impl Iterator<Item = &'a str>,
    ) -> Option<&mut Self> {
        let mut section = self;

        for section_name in section_path {
            section = section.sections.get_mut(section_name)?;
        }

        Some(section)
    }
}

impl EntryPath {
    pub fn new(section_path: Vec<String>, entry_name: String) -> Self {
        Self {
            section_path,
            entry_name,
        }
    }
}

impl TryFrom<&str> for EntryPath {
    type Error = ();

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let mut sections: Vec<_> = value.split('.').map(ToOwned::to_owned).collect();
        let entry_name = sections.pop().ok_or(())?;

        Ok(Self {
            section_path: sections,
            entry_name,
        })
    }
}
