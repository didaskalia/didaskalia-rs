use crate::entry_collection::EntryPath;
use std::{
    borrow::Cow,
    fmt::{self, Display, Formatter},
};

pub trait TokenValue<'a>: TryFrom<Token<'a>> {
    fn kind() -> TokenKind;
}

create_tokens! {
    Identifier<'a>(pub text: &'a str) -> create_identifier = ("identifier"" `{text}`"),

    Str<'a>(pub string: Option<UnparsedStringExpression<'a>>) -> create_string = ("string"" {}",
        match string {
            Some(string) => format!("{}", string),
            None => String::from("invalid"),
        }
    ),

    Dot -> create_dot = ("'.'"),

    LeftBracket -> create_left_bracket = ("'['"),
    RightBracket -> create_right_bracket = ("']'"),

    Equal -> create_equal = ("'='"),

    Newline -> create_newline = ("'\\n'"),

    InvalidCharacter(pub c: char) -> create_invalid_character = ("invalid character"" {c:?}"),
}

#[derive(Debug, Clone)]
pub struct UnparsedStringExpression<'a>(pub Vec<(Cow<'a, str>, Vec<Token<'a>>)>, pub Cow<'a, str>);

#[derive(Debug)]
pub struct StringExpression(pub Vec<(String, EntryPath)>, pub String);

impl<'a> Display for UnparsedStringExpression<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "\"")?;

        for (s, tokens) in &self.0 {
            write!(f, "{s:?}[")?;

            if let Some(token) = tokens.first() {
                write!(f, "{}", token)?;
            }
            for token in tokens.iter().skip(1) {
                write!(f, " {}", token)?;
            }

            write!(f, "]")?;
        }

        write!(f, "{:?}", self.1)?;

        write!(f, "\"")
    }
}

impl StringExpression {
    pub fn is_empty(&self) -> bool {
        self.0.is_empty() && self.1.is_empty()
    }

    pub fn is_trimmed_empty(&self) -> bool {
        self.0.is_empty() && self.1.trim().is_empty()
    }
}

macro_rules! first {
    ($first:tt $($tt:tt)*) => {
        $first
    };
}
macro_rules! create_tokens {
    (
        $(
            $variant_name:ident
            $(<$variant_lifetime:lifetime>)?

            $(
                ($($field_vis:vis $field_name:ident : $field_type:ty),* $(,)?)
            )?

            -> $variant_creation_fn_name:ident
            = ( $token_kind_display:literal $($($format_arg:expr),+)?$(,)? )
        ),* $(,)?
    ) => {
        #[derive(Debug, Clone)]
        pub enum Token<'a> {
            $($variant_name($variant_name $(<$variant_lifetime>)?),)*
        }

        impl<'a> Token<'a> {
            pub fn kind(&self) -> TokenKind {
                match self {
                    $(
                        Self::$variant_name(_) => TokenKind::$variant_name,
                    )*
                }
            }

            pub fn span(&self) -> $crate::span::Span {
                match self {
                    $(
                        Self::$variant_name(v) => v.span,
                    )*
                }
            }

            $(
                #[allow(unused)]
                pub fn $variant_creation_fn_name(
                    $( $($field_name: $field_type,)* )?
                    span: $crate::span::Span
                ) -> Token<'a>
                {
                    Self::$variant_name($variant_name {
                        $(
                            $($field_name,)*
                        )?
                        span
                    })
                }
            )*
        }

        impl<'a> std::fmt::Display for Token<'a> {
            fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
                match self {
                    $(
                        Self::$variant_name($variant_name {
                            $(
                                $($field_name,)*
                            )?
                            ..
                        }) => write!(
                            fmt,
                            "{}{}",
                            $token_kind_display,
                            first!(
                                $((format!($($format_arg),+)))?
                                ""
                            )
                        ),
                    )*
                }
            }
        }

        impl std::fmt::Display for TokenKind {
            fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
                match self {
                    $(
                        Self::$variant_name => write!(fmt, $token_kind_display),
                    )*
                }
            }
        }

        #[derive(Debug, Clone, Copy, PartialEq, Eq)]
        pub enum TokenKind {
            $($variant_name,)*
        }

        $(
            #[derive(Debug, Clone)]
            pub struct $variant_name $(<$variant_lifetime>)? {
                $( $($field_vis $field_name: $field_type,)* )?
                pub span: $crate::span::Span,
            }

            impl<'a> TokenValue<'a> for $variant_name $(<$variant_lifetime>)? {
                fn kind() -> TokenKind {
                    TokenKind::$variant_name
                }
            }

            impl<'a> TryFrom<Token<'a>> for $variant_name $(<$variant_lifetime>)? {
                type Error = ();

                fn try_from(token: Token<'a>) -> Result<Self, Self::Error> {
                    match token {
                        Token::$variant_name(value) => Ok(value),
                        _ => Err(()),
                    }
                }
            }
        )*
    };
}

use {create_tokens, first};
