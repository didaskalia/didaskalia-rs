use std::{iter::Peekable, str::Chars};

use crate::{char_ext::CharExt, pos::Pos};

#[derive(Clone)]
pub struct Cursor<'a> {
    chars: Peekable<Chars<'a>>,
    pos: Pos,
}

impl<'a> Cursor<'a> {
    pub fn new(source: &'a str) -> Self {
        let chars = source.chars().peekable();

        Self {
            chars,
            pos: Pos::ZERO,
        }
    }

    pub fn new_at(source: &'a str, pos: Pos) -> Self {
        let chars = source[pos.index..].chars().peekable();

        Self { chars, pos }
    }

    pub fn pos(&self) -> Pos {
        self.pos
    }

    pub fn consume(&mut self) {
        self.next();
    }

    pub fn peek(&mut self) -> Option<char> {
        self.chars.peek().copied()
    }

    pub fn peek_then_iter<'b>(&'b mut self) -> CursorIterThenPeek<'a, 'b> {
        CursorIterThenPeek::new(self)
    }

    pub fn consume_while<P: FnMut(char) -> bool>(&mut self, mut predicate: P) {
        self.peek_then_iter().find(|c| !predicate(*c));
    }

    pub fn try_parse(&mut self, s: &str) -> bool {
        let mut cursor = self.clone();

        if cursor.by_ref().take(s.len()).eq(s.chars()) {
            *self = cursor;
            true
        } else {
            false
        }
    }

    pub fn matches(&self, s: &str) -> bool {
        self.chars.clone().take(s.len()).eq(s.chars())
    }

    pub fn skip_whitespace(&mut self) {
        self.consume_while(char::is_non_newline_whitespace);
    }
}

impl<'a> Iterator for Cursor<'a> {
    type Item = char;

    fn next(&mut self) -> Option<Self::Item> {
        let Some(c) = self.chars.next() else {
            return None;
        };

        self.pos = self.pos.advance(c);

        Some(c)
    }
}

pub struct CursorIterThenPeek<'a, 'b> {
    cursor: &'b mut Cursor<'a>,
    first_char: bool,
}

impl<'a, 'b> CursorIterThenPeek<'a, 'b> {
    fn new(cursor: &'b mut Cursor<'a>) -> Self {
        Self {
            cursor,
            first_char: true,
        }
    }
}

impl<'a, 'b> Iterator for CursorIterThenPeek<'a, 'b> {
    type Item = char;

    fn next(&mut self) -> Option<Self::Item> {
        if self.first_char {
            self.first_char = false;
        } else {
            self.cursor.consume();
        }
        self.cursor.peek()
    }
}
