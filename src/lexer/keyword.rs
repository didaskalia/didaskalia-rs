#[derive(Debug, Copy, Clone)]
pub enum Keyword {
    Lang,
    Global,
    Forbid,
    Warn,
    Inform,
    Ignore,
    Section,
    End,
}

impl Keyword {
    pub fn as_str(self) -> &'static str {
        match self {
            Keyword::Lang => "lang",
            Keyword::Global => "global",
            Keyword::Forbid => "forbid",
            Keyword::Warn => "warn",
            Keyword::Inform => "inform",
            Keyword::Ignore => "ignore",
            Keyword::Section => "section",
            Keyword::End => "end",
        }
    }
}
