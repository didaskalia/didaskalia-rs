use std::{
    fmt::Display,
    io::{stderr, IsTerminal},
    ops::Deref,
};

use colored::{ColoredString, Colorize};

use crate::{
    lexer::{
        keyword::Keyword,
        token::{Token, TokenKind},
    },
    span::Span,
};

#[derive(Debug, Default)]
pub struct DiagnosticList<'a> {
    list: Vec<Diagnostic<'a>>,
    context: Context<'a>,

    lint_severities: LintSeverities,

    unwrap_try_depth: usize,
    try_depth: usize,
}

create_diagnostics! {
    Lexer: lexer {
        ContextKind<'a> {
            ExpectingToken(kind: TokenKind) => ("expecting {kind} token"),
            ExpectingKeyword(keyword: Keyword) => ("expecting keyword `{}`", keyword.as_str()),
            ExpectingCharacter(c: char) => ("expecting character {c:?}"),
            ExpectingEOF => ("expecting eof-of-file"),
            Expecting(something: &'a str) => ("expecting {something}"),

            ParsingDelimitedString => ("parsing delimited string"),
            ParsingUndelimitedString => ("parsing undelimited string"),
            ParsingUnicodeEscapeSequence => ("parsing unicode escape sequence"),
        }
        LintKind {}
        #[allow(clippy::upper_case_acronyms)]
        Error<'a> {
            EOF => ("prematurely reached end-of-file"),
            UnexpectedCharacter(c: char) => ("unexpected character {c:?}"),
            UnexpectedToken(token: Token<'a>) => ("unexpected {token}"),

            EmptyValue => ("empty value"),
            InvalidHexNumber(value: &'a str) => ("{value:?} is not a valid hex number"),
            InvalidUnicode(value: &'a str) => ("{value:?} is not a valid unicode character"),
            UnicodeEscapeSequenceTooManyDigits => ("expected at most 6 digits"),

            UnclosedMultilineComment => ("unclosed multiline comment"),
        }
    }
    Parser: parser {
        ContextKind<'a> {
            ParsingString => ("parsing string"),

            EntryDeclaration => ("parsing entry declaration"),
            EntryDeclarationWithName(name: &'a str) => ("parsing entry declaration [{}]", name.escape_debug()),

            LintDirective(keyword: Keyword) => ("parsing `{}` lint directive", keyword.as_str()),
            GlobalLintDirective => ("parsing `global` lint directive"),
            GlobalLintDirectiveWithKeyword(keyword: Keyword) => ("parsing `global {}` lint directive", keyword.as_str()),

            LangDirective => ("parsing lang directive"),
            MisplacedLangDirective => ("parsing misplaced lang directive"),

            SectionOpening => ("parsing opening of new section"),
            SectionInside(section: &'a str) => ("parsing inside section '{section}'"),
            SectionEnding(section: &'a str) => ("parsing ending of section '{section}'"),
        }
        LintKind {
            EmptyEntryValue as empty_entry_value = Warning => ("entry has an empty value"),
            BlankEntryValue as blank_entry_value = Warning => ("entry has a non-empty but blank value"),

            EmptySection as empty_section = Warning => ("this section is empty and can be removed"),
        }
        #[allow(clippy::upper_case_acronyms)]
        Error<'a> {
            EOF => ("prematurely reached end-of-file"),
            UnexpectedToken(token: Token<'a>) => ("unexpected {token}"),

            MissingLangDirective => ("file must start with a `lang` directive to indicate its language"),
            MisplacedLangDirective => ("the `lang` directive must only appear at the beginning of the file"),

            InvalidGlobalLintDirective => ("`global` keyword must be followed by a lint directive, i.e. either the `forbid`, `warn`, `inform` or `ignore` keyword"),
            InvalidLintID(id: &'a str) => ("there does not exist a lint with an id of '{id}'"),
            LintDiretivesMustBeFollowedByStatement => ("`lint` directives must be followed by a statement"),

            EntryAlreadyDeclared => ("entry has already been declared before"),

            SectionLeftUnclosed(section: &'a str) => ("section '{section}' was opened then left unclosed for the entirety of the file"),
            InvalidSectionEnd => ("attempted to close section where none is currently opened"),
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub enum Severity {
    Error,
    Warning,
    Info,
}

#[derive(Debug)]
pub struct Diagnostic<'a> {
    kind: DiagnosticKind<'a>,
    context: Context<'a>,
    span: Span,
}

pub(crate) trait LintKind<'a> {
    fn into_diagnostic_kind(self, severity: Severity) -> DiagnosticKind<'a>;
    fn id(&self) -> &str;
}

#[derive(Debug, Clone, Default)]
pub struct Context<'a>(Vec<ContextKind<'a>>);

impl<'a> DiagnosticList<'a> {
    pub fn new() -> Self {
        Self::default()
    }

    pub(crate) fn enter_context<C: Into<ContextKind<'a>>>(&mut self, context: C) {
        self.context.0.push(context.into());
    }

    pub(crate) fn exit_context(&mut self) {
        if self.context.0.pop().is_none() {
            panic!("Exited context while none where entered before.");
        };
    }

    pub(crate) fn replace_context<C: Into<ContextKind<'a>>>(&mut self, context: C) {
        self.exit_context();
        self.enter_context(context);
    }

    pub(crate) fn enter_try(&mut self) {
        self.try_depth += 1;
    }

    pub(crate) fn exit_try(&mut self) {
        if self.try_depth == 0 {
            panic!("Exited try while none where entered before.");
        };

        self.try_depth -= 1;
    }

    pub(crate) fn enter_unwrap_try(&mut self) {
        self.unwrap_try_depth += 1;
    }

    pub(crate) fn exit_unwrap_try(&mut self) {
        if self.unwrap_try_depth == 0 {
            panic!("Exited unwrap_try while none where entered before.");
        }

        self.unwrap_try_depth -= 1;
    }

    pub(crate) fn open_lint_scope(&mut self) {
        self.lint_severities.open_scope();
    }

    pub(crate) fn close_lint_scope(&mut self) {
        self.lint_severities.close_scope();
    }

    pub(crate) fn flush_lint_overrides(&mut self) {
        self.lint_severities.flush_overrides();
    }

    pub(crate) fn verify_all_lint_scopes_closed(&mut self) {
        self.lint_severities.verify_all_scopes_closed();
    }

    pub(crate) fn override_lint_severity(&mut self, id: &str, severity: Option<Severity>) -> bool {
        self.lint_severities.set_severity(id, severity)
    }

    pub(crate) fn add_lint<L: LintKind<'a>>(&mut self, lint: L, span: Span) {
        if let Some(severity) = self.lint_severities.get_severity(lint.id()).unwrap() {
            self.add(lint.into_diagnostic_kind(severity), span);
        }
    }

    pub(crate) fn add<D: Into<DiagnosticKind<'a>>>(&mut self, diagnostic: D, span: Span) {
        if let Some(diagnostic) = self.create_diagnostic(diagnostic.into(), span) {
            self.add_diagnostic(diagnostic);
        }
    }

    pub(crate) fn add_diagnostic(&mut self, diagnostic: Diagnostic<'a>) {
        self.list.push(diagnostic);
    }

    pub(crate) fn create_diagnostic<D: Into<DiagnosticKind<'a>>>(
        &self,
        diagnostic: D,
        span: Span,
    ) -> Option<Diagnostic<'a>> {
        if self.try_depth > self.unwrap_try_depth {
            None
        } else {
            Some(Diagnostic {
                kind: diagnostic.into(),
                context: self.context.clone(),
                span,
            })
        }
    }

    pub(crate) fn add_to<D: Into<DiagnosticKind<'a>>>(
        &self,
        diagnostics: &mut Vec<Diagnostic<'a>>,
        diagnostic: D,
        span: Span,
    ) {
        if let Some(diagnostic) = self.create_diagnostic(diagnostic, span) {
            diagnostics.push(diagnostic)
        }
    }

    pub(crate) fn check_exited_all_contexts(&self) {
        if !self.context.0.is_empty() {
            panic!(
                "Some contexts where not exited:\n{}\n",
                self.context.0[0].message()
            );
        }
    }

    pub fn print(&self, source: &str) {
        self.check_exited_all_contexts();

        self.list
            .iter()
            .for_each(|diagnostic| diagnostic.print(source));
    }
}

impl<'a> Deref for DiagnosticList<'a> {
    type Target = [Diagnostic<'a>];

    fn deref(&self) -> &Self::Target {
        &self.list
    }
}

macro_rules! with_context {
    (
        $diagnostics:expr; $context:expr => $code:expr
    ) => {{
        $diagnostics.borrow_mut().enter_context($context);
        #[allow(clippy::redundant_closure_call)]
        let ret = (|| $code)();
        $diagnostics.borrow_mut().exit_context();
        ret
    }};
}

macro_rules! with_try {
    (
        $diagnostics:expr => $code: expr
    ) => {{
        $diagnostics.borrow_mut().enter_try();
        #[allow(clippy::redundant_closure_call)]
        let ret = (|| $code)();
        $diagnostics.borrow_mut().exit_try();
        ret
    }};
}

macro_rules! with_try_context {
    (
        $diagnostics:expr; $context:expr => $code:expr
    ) => {
        $crate::diagnostics::with_try!($diagnostics => $crate::diagnostics::with_context!($diagnostics; $context => $code))
    };
}

macro_rules! unwrap_try {
    (
        $diagnostics:expr => $code: expr
    ) => {{
        $diagnostics.borrow_mut().enter_unwrap_try();
        #[allow(clippy::redundant_closure_call)]
        let ret = (|| $code)();
        $diagnostics.borrow_mut().exit_unwrap_try();
        ret
    }};
}

pub(crate) use {unwrap_try, with_context, with_try, with_try_context};

impl<'a> Diagnostic<'a> {
    pub fn kind(&self) -> &DiagnosticKind<'a> {
        &self.kind
    }

    pub fn context(&self) -> &Context<'a> {
        &self.context
    }

    pub fn span(&self) -> Span {
        self.span
    }

    fn print(&self, source: &str) {
        colored::control::set_override(stderr().is_terminal());

        let mut span = self.span;

        // if the span ends with a \n, go back one character.
        if source[..span.end.index].ends_with('\n') {
            span.end.line -= 1;
            span.end.index -= 1;

            // if crlf, go back one more character.
            if source[..span.end.index].ends_with('\r') {
                span.end.index -= 1;
            }

            let start_line_index = source[..span.end.index]
                .rfind('\n')
                .map(|n| n + 1)
                .unwrap_or(0);
            span.end.column = source[start_line_index..span.end.index].chars().count();
        }

        // if the end of the span was moved before the start, make it into an empty span.
        if span.start.index > span.end.index {
            span.start = span.end;
        }

        let first_line_index = source[..span.start.index]
            .rfind('\n')
            .map(|n| n + 1)
            .unwrap_or(0);
        let last_line_index = source[span.end.index..]
            .find('\n')
            .map(|end| end + span.end.index)
            // if crlf, go back one more character.
            .map(|end| {
                if end > 0 && source[end - 1..].starts_with('\r') {
                    end - 1
                } else {
                    end
                }
            })
            .unwrap_or(source.len());

        let line_number_len = (span.end.line + 1).to_string().len();
        let code_slice = &source[first_line_index..last_line_index];

        let sep = "║".dimmed();

        let no_line_space = " ".repeat(line_number_len);
        let no_line_number = &format!("{} {}", no_line_space, sep);
        let ctx_arrow = format!("{}-->", no_line_space);

        let severity = self.kind.severity();
        eprintln!("{}", severity.stylize(&format!("[{}]", severity)));

        //eprint!("{}", self.context.message());
        for kind in &self.context.0 {
            eprintln!("{ctx_arrow} {}", kind.message());
        }

        if span.start.line == span.end.line {
            eprintln!("{}", no_line_number);
            eprintln!(
                "{:>line_number_len$} {} {}",
                span.start.line + 1,
                sep,
                code_slice
            );

            eprintln!(
                "{}{}{}",
                no_line_number,
                " ".repeat(span.start.column),
                if span.is_empty() {
                    severity.stylize(" ↑")
                } else {
                    severity.stylize(&format!(
                        "└┬{}┘",
                        "─".repeat(span.end.column - span.start.column - 1)
                    ))
                }
            );

            eprint!("{}{}", no_line_number, " ".repeat(span.start.column + 1));
            if !span.is_empty() {
                eprint!("{}", severity.stylize("└─ "));
            }
            eprintln!("{}", severity.stylize(&self.kind.message()));

            eprintln!("{}", no_line_number);
        } else {
            let max_line_len = code_slice
                .lines()
                .map(|line| line.chars().count())
                .max()
                .unwrap();

            eprintln!(
                "{} {}{}",
                no_line_number,
                " ".repeat(max_line_len),
                severity.stylize("─┐")
            );

            let bar = severity.stylize("│\n");
            let msg = severity.stylize(format!("├─ {}\n", self.kind.message()).as_str());
            for (line_number, line) in
                std::iter::zip(span.start.line..=span.end.line, code_slice.lines())
            {
                eprint!(
                    "{:>line_number_len$} {} {line:<max_line_len$} {}",
                    line_number + 1,
                    sep,
                    if line_number == span.end.line {
                        &msg
                    } else {
                        &bar
                    }
                );
            }

            eprintln!(
                "{} {}{}",
                no_line_number,
                " ".repeat(max_line_len),
                severity.stylize("─┘")
            );
        }

        colored::control::unset_override();
    }
}

impl<'a> Context<'a> {
    pub fn kinds(&self) -> &[ContextKind<'a>] {
        &self.0
    }

    /*
       NOTE: commented because now unused.
       for pretty printing, we need more info in order to display the context
       (we use the length of the max line number to offset the arrows that preceed context messages)
       because we probably don't want to add those values as arguments in this public-api function,
       it will stay commented until new changes adress this matter.
    */
    // pub fn message(&self) -> String {
    //     let mut s = String::new();
    //
    //     for context in &self.0 {
    //         s.push_str("\t while ");
    //         s.push_str(&context.message());
    //         s.push_str(":\n");
    //     }
    //
    //     s
    // }
}

impl Severity {
    fn stylize(&self, s: &str) -> ColoredString {
        (match self {
            Self::Error => Colorize::bright_red,
            Self::Warning => Colorize::bright_yellow,
            Self::Info => Colorize::bright_green,
        }(s))
    }
}

impl Display for Severity {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Error => "ERROR",
                Self::Warning => "WARNING",
                Self::Info => "INFO",
            }
        )
    }
}

macro_rules! create_diagnostics {
    (
        $(
            $(#[$mod_meta:meta])*
            $Mod:ident: $module:ident {
                $(#[$context_meta:meta])*
                ContextKind $(<$context_lifetime:lifetime>)? {
                    $($context_content:tt)*
                }$(,)?

                LintKind $(<$lint_lifetime:lifetime>)? {
                    $($lint_content:tt)*
                }

                $(
                    $(#[$severity_meta:meta])*
                    $Severity:ident $(<$severity_lifetime:lifetime>)? {
                        $($severity_content:tt)*
                    }$(,)?
                )*
            }$(,)?
        )*
    ) => {
        create_diagnostics! {
            @with_lifetimes

            ContextKind <$($($context_lifetime)?)*>,
            DiagnosticKind <$($($($severity_lifetime)?)*)*> {
                $(
                    $(#[$mod_meta])*
                    $Mod: $module {
                        $(#[$context_meta])*
                        ContextKind $(<$context_lifetime>)? {
                            $($context_content)*
                        }

                        LintKind $(<$lint_lifetime>)? {
                            $($lint_content)*
                        }

                        DiagnosticKind <$($($severity_lifetime)?)*> {
                            $(
                                $(#[$severity_meta])*
                                $Severity $(<$severity_lifetime>)? {
                                    $($severity_content)*
                                }
                            )*
                        }
                    }
                )*
            }
        }
    };

    (
        @with_lifetimes
        ContextKind <$($global_context_lifetime:lifetime $($_:lifetime)*)?>,
        DiagnosticKind <$($global_diagnostic_lifetime:lifetime $($___:lifetime)*)?> $(,)? {
            $(
                $(#[$mod_meta:meta])*
                $Mod:ident: $module:ident {
                    $(#[$context_meta:meta])*
                    ContextKind $(<$context_lifetime:lifetime>)? {
                        $(
                            $Context:ident
                                $((
                                    $($context_unnamed_name:ident: $ContextUnnamedType:ty),*$(,)?
                                ))?
                                $({
                                    $($context_named_name:ident: $ContextNamedType:ty),*$(,)?
                                })?
                                    => ($($context_fmt_arg:expr),*$(,)?)
                        ),*$(,)?
                    }$(,)?

                    LintKind $(<$lint_lifetime:lifetime>)? {
                        $(
                            $Lint:ident as $lint_id:ident $(= $LintSeverity:ident)?
                                $((
                                    $($lint_unnamed_name:ident: $LintUnnamedType:ty),*$(,)?
                                ))?
                                $({
                                    $($lint_named_name:ident: $LintNamedType:ty),*$(,)?
                                })?
                                    => ($($lint_fmt_arg:expr),*$(,)?)
                        ),*$(,)?
                    }

                    DiagnosticKind <$($diagnostic_lifetime:lifetime $($____:lifetime)*)?> {
                        $(
                            $(#[$severity_meta:meta])*
                            $Severity:ident $(<$severity_lifetime:lifetime>)? {
                                $(
                                    $Diagnostic:ident
                                        $((
                                            $($diagnostic_unnamed_name:ident: $DiagnosticUnnamedType:ty),*$(,)?
                                        ))?
                                        $({
                                            $($diagnostic_named_name:ident: $DiagnosticNamedType:ty),*$(,)?
                                        })?
                                            => ($($diagnostic_fmt_arg:expr),*$(,)?)
                                ),*$(,)?
                            }$(,)?
                        )*
                    }
                }$(,)?
            )*
        }
    ) => {
        #[derive(Debug)]
        pub enum DiagnosticKind $(<$global_diagnostic_lifetime>)? {
            $(
                $Mod($module::DiagnosticKind $(<$diagnostic_lifetime>)?)
            ),*
        }
        type _DiagnosticKind<'a> = DiagnosticKind $(<$global_diagnostic_lifetime>)?;

        #[derive(Debug, Clone)]
        pub enum ContextKind $(<$global_context_lifetime>)? {
            $(
                $Mod($module::ContextKind $(<$context_lifetime>)?)
            ),*
        }
        type _ContextKind<'a> = ContextKind $(<$global_context_lifetime>)?;

        #[derive(Debug)]
        pub(crate) struct LintOverrides {
            $(
                $(
                    $lint_id: Option<Option<Severity>>,
                )*
            )*
        }

        #[derive(Debug)]
        pub(crate) struct LintSeverities {
            overrides: Vec<LintOverrides>,
        }

        $(
            pub mod $module {
                use super::*;

                $(#[$mod_meta])*
                #[derive(Debug)]
                pub enum DiagnosticKind $(<$diagnostic_lifetime>)? {
                    $(
                        $Severity($Severity $(<$severity_lifetime>)?),
                    )*
                    Lint(LintKind $(<$lint_lifetime>)?, Severity),
                }
                type _DiagnosticKind<'a> = DiagnosticKind $(<$diagnostic_lifetime>)?;

                #[derive(Debug)]
                pub enum LintKind $(<$lint_lifetime>)? {
                    $(
                        $Lint
                        $((
                            $($LintUnnamedType),*
                        ))?
                        $({
                            $($lint_named_name: $LintNamedType),*
                        })?
                    ),*
                }

                $(#[$context_meta])*
                #[derive(Debug, Clone)]
                pub enum ContextKind $(<$context_lifetime>)? {
                    $(
                        $Context
                        $((
                            $($ContextUnnamedType),*
                        ))?
                        $({
                            $($context_named_name: $ContextNamedType),*
                        })?
                    ),*
                }
                type _ContextKind<'a> = ContextKind $(<$context_lifetime>)?;

                $(
                    $(#[$severity_meta])*
                    #[derive(Debug)]
                    pub enum $Severity $(<$severity_lifetime>)? {
                        $(
                            $Diagnostic
                            $((
                                $($DiagnosticUnnamedType),*
                            ))?
                            $({
                                $($diagnostic_named_name: $DiagnosticNamedType),*
                            })?
                        ),*
                    }

                    impl $(<$severity_lifetime>)? $Severity $(<$severity_lifetime>)? {
                        #[allow(unreachable_patterns)]
                        pub fn message(&self) -> String {
                            match self {
                                $(
                                    Self::$Diagnostic
                                        $(($($diagnostic_unnamed_name),*))?
                                        $({$($diagnostic_named_name),*})?
                                            => format!($($diagnostic_fmt_arg),*),
                                )*
                                _ => unreachable!(),
                            }
                        }
                    }

                    impl<'a> From<$Severity $(<$severity_lifetime>)?> for _DiagnosticKind<'a> {
                        fn from(value: $Severity $(<$severity_lifetime>)?) -> Self {
                            Self::$Severity(value)
                        }
                    }

                    impl<'a> From<$Severity $(<$severity_lifetime>)?> for super::_DiagnosticKind<'a> {
                        fn from(value: $Severity $(<$severity_lifetime>)?) -> Self {
                            _DiagnosticKind::from(value).into()
                        }
                    }
                )*

                impl $(<$lint_lifetime>)? LintKind $(<$lint_lifetime>)? {
                    #[allow(unreachable_patterns)]
                    pub fn message(&self) -> String {
                        match self {
                            $(
                                Self::$Lint
                                    $(($($lint_unnamed_name),*))?
                                    $({$($lint_named_name),*})?
                                        => format!($($lint_fmt_arg),*),
                            )*
                            _ => unreachable!(),
                        }
                    }
                }

                impl<'a> super::LintKind<'a> for LintKind$(<$lint_lifetime>)? {
                    fn into_diagnostic_kind(self, severity: Severity) -> super::DiagnosticKind<'a> {
                        DiagnosticKind::Lint(self, severity).into()
                    }

                    #[allow(unreachable_patterns)]
                    fn id(&self) -> &str {
                        match self {
                            $(
                                Self::$Lint
                                    $(($($lint_unnamed_name),*))?
                                    $({$($lint_named_name),*})?
                                        => stringify!($lint_id),
                            )*
                            _ => unreachable!(),
                        }
                    }
                }

                impl $(<$context_lifetime>)? ContextKind $(<$context_lifetime>)? {
                    #[allow(unreachable_patterns)]
                    pub fn message(&self) -> String {
                        match self {
                            $(
                                Self::$Context
                                    $(($($context_unnamed_name),*))?
                                    $({$($context_named_name),*})?
                                        => format!($($context_fmt_arg),*),
                            )*
                            _ => unreachable!(),
                        }
                    }
                }

                impl<'a> From<_ContextKind<'a>> for super::_ContextKind<'a> {
                    fn from(value: _ContextKind<'a>) -> Self {
                        Self::$Mod(value)
                    }
                }

                impl $(<$diagnostic_lifetime>)? DiagnosticKind $(<$diagnostic_lifetime>)? {
                    #[allow(unreachable_patterns)]
                    pub fn message(&self) -> String {
                        match self {
                            $(
                                Self::$Severity(severity_diagnostic) => severity_diagnostic.message(),
                            )*
                            Self::Lint(lint, _) => lint.message(),
                            _ => unreachable!(),
                        }
                    }

                    #[allow(unreachable_patterns)]
                    pub fn severity(&self) -> Severity {
                        match self {
                            $(
                                Self::$Severity(_) => Severity::$Severity,
                            )*
                            Self::Lint(_, severity) => *severity,
                            _ => unreachable!(),
                        }
                    }
                }

                impl<'a> From<_DiagnosticKind<'a>> for super::_DiagnosticKind<'a> {
                    fn from(value: _DiagnosticKind<'a>) -> Self {
                        Self::$Mod(value)
                    }
                }
            }
        )*

        impl $(<$global_diagnostic_lifetime>)? DiagnosticKind $(<$global_diagnostic_lifetime>)? {
            #[allow(unreachable_patterns)]
            pub fn message(&self) -> String {
                match self {
                    $(
                        Self::$Mod(mod_diagnostic) => mod_diagnostic.message(),
                    )*
                    _ => unreachable!(),
                }
            }

            #[allow(unreachable_patterns)]
            pub fn severity(&self) -> Severity {
                match self {
                    $(
                        Self::$Mod(mod_diagnostic) => mod_diagnostic.severity(),
                    )*
                    _ => unreachable!(),
                }
            }
        }

        impl $(<$global_context_lifetime>)? ContextKind $(<$global_context_lifetime>)? {
            #[allow(unreachable_patterns)]
            pub fn message(&self) -> String {
                match self {
                    $(
                        Self::$Mod(context) => context.message(),
                    )*
                    _ => unreachable!(),
                }
            }
        }

        impl LintOverrides {
            pub fn new() -> Self {
                Self {
                    $(
                        $(
                            $lint_id: None,
                        )*
                    )*
                }
            }

            pub fn get_severity(&self, id: &str) -> Option<Option<Option<Severity>>> {
                match id {
                    $(
                        $(
                            stringify!($lint_id) => Some(self.$lint_id),
                        )*
                    )*
                    _ => None,
                }
            }

            pub fn set_severity(&mut self, id: &str, severity: Option<Severity>) -> bool {
                match id {
                    $(
                        $(
                            stringify!($lint_id) => { self.$lint_id = Some(severity); true },
                        )*
                    )*
                    _ => false,
                }
            }
        }

        impl LintSeverities {
            fn new() -> Self {
                Self { overrides: Vec::new() }
            }

            fn open_scope(&mut self) {
                self.overrides.push(LintOverrides::new());
            }

            fn close_scope(&mut self) {
                self.overrides.pop().expect("trying to close a scope when too few were opened!");
            }

            fn flush_overrides(&mut self) {
                self.close_scope();
                self.open_scope();
            }

            fn verify_all_scopes_closed(&self) {
                assert!(self.overrides.is_empty(), "some scopes were not closed!");
            }

            // returns None if id does not exist
            fn get_severity(&self, id: &str) -> Option<Option<Severity>> {
                for lint_overrides in self.overrides.iter().rev() {
                    match lint_overrides.get_severity(id) {
                        None => return None, // id does not exist, stop searching - it's pointless
                        Some(None) => continue, // id was not overriden, keep searching
                        Some(Some(severity)) => return Some(severity), // found severity
                    }
                }

                Some(match id {
                    $(
                        $(
                            stringify!($lint_id) => None$(.unwrap_or(Some(Severity::$LintSeverity)))?,
                        )*
                    )*
                    _ => unreachable!(),
                })
            }

            // returns false if id does not exist
            fn set_severity(&mut self, id: &str, severity: Option<Severity>) -> bool {
                let last = self.overrides
                    .last_mut()
                    .expect("trying to override lint severity when no scope was opened!");
                last.set_severity(id, severity)
            }
        }

        impl Default for LintSeverities {
            fn default() -> Self {
                Self::new()
            }
        }
    };
}

use create_diagnostics;
