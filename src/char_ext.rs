#[allow(clippy::wrong_self_convention)]
pub trait CharExt {
    fn is_newline(self) -> bool;
    fn is_non_newline_whitespace(self) -> bool;

    fn is_identifier_head(self) -> bool;
    fn is_identifier_tail(self) -> bool;
}

impl CharExt for char {
    fn is_newline(self) -> bool {
        self == '\n'
    }
    fn is_non_newline_whitespace(self) -> bool {
        self.is_whitespace() && !self.is_newline()
    }

    fn is_identifier_head(self) -> bool {
        self.is_alphabetic() || self == '_'
    }
    fn is_identifier_tail(self) -> bool {
        self.is_alphanumeric() || self == '_'
    }
}
