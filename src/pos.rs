use std::fmt::Display;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Pos {
    pub index: usize,
    pub line: usize,
    pub column: usize,
}

impl Pos {
    pub const ZERO: Self = Self::new(0, 0, 0);

    pub const fn new(index: usize, line: usize, column: usize) -> Self {
        Self {
            index,
            line,
            column,
        }
    }

    pub const fn advance(mut self, next: char) -> Self {
        self.index += next.len_utf8();

        if next == '\n' {
            self.line += 1;
            self.column = 0;
        } else {
            self.column += 1;
        }

        self
    }

    /// WARNING: If the previous character is a newline, this will panic !
    pub const fn backtrack(mut self, prev: char) -> Self {
        if prev == '\n' || self.column == 0 {
            panic!("Called `Pos::backtrack` the previous character being a newline.");
        }

        self.index -= prev.len_utf8();
        self.column -= 1;

        self
    }
}

impl Display for Pos {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}:{})", self.line + 1, self.column + 1)
    }
}
