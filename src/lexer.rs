use self::{
    cursor::Cursor,
    keyword::Keyword,
    token::{Identifier, Str, Token, TokenKind, TokenValue, UnparsedStringExpression},
};

use crate::{
    char_ext::CharExt,
    diagnostics::{
        lexer::{ContextKind, Error},
        unwrap_try, with_context, with_try, with_try_context, Diagnostic, DiagnosticList,
    },
    pos::Pos,
    span::Span,
};

use std::{borrow::Cow, cell::RefCell, collections::VecDeque, rc::Rc};

mod cursor;
pub mod keyword;
pub mod token;

pub struct Lexer<'a> {
    source: &'a str,
    cursor: Cursor<'a>,

    diagnostics: Rc<RefCell<DiagnosticList<'a>>>,
    eof_diagnostics: Vec<Diagnostic<'a>>,

    incoming_tokens: VecDeque<(Token<'a>, Vec<Diagnostic<'a>>)>,
}

impl<'a> Lexer<'a> {
    pub fn new(source: &'a str, diagnostics: Rc<RefCell<DiagnosticList<'a>>>) -> Self {
        Self {
            source,
            cursor: Cursor::new(source),
            diagnostics,
            eof_diagnostics: Vec::new(),
            incoming_tokens: VecDeque::new(),
        }
    }

    pub fn pos(&self) -> Pos {
        self.incoming_tokens
            .get(0)
            .map(|token| token.0.span().start)
            .unwrap_or(self.cursor.pos())
    }

    // returns a clone of the token at the lexer's position, + some offset forward
    // the lexer may push all unread tokens up to the specified index if necessary
    pub fn peek(&mut self, at: usize) -> Option<Token<'a>> {
        for _ in self.incoming_tokens.len()..=at {
            if self.cursor.peek().is_none() {
                break;
            }

            let mut token_diagnostics = Vec::new();
            let Some(token) = self.lex(&mut token_diagnostics) else {
                self.eof_diagnostics = token_diagnostics;
                return None;
            };

            self.incoming_tokens.push_back((token, token_diagnostics));
        }

        self.incoming_tokens.get(at).map(|tuple| tuple.0.clone())
    }

    // advances the token index
    // lexing will occur on .peek(...) call
    pub fn consume(&mut self) {
        if self.peek(0).is_none() {
            return;
        }

        let token_diagnostics = self.incoming_tokens.pop_front().unwrap().1;
        let mut diagnostics = self.diagnostics.borrow_mut();
        for diagnostic in token_diagnostics {
            diagnostics.add_diagnostic(diagnostic);
        }
    }

    // consumes peeked tokens without emitting token-associated errors
    pub fn consume_quietly(&mut self) {
        if self.peek(0).is_some() {
            self.incoming_tokens.pop_front();
        }
    }

    pub fn consume_string(&mut self) -> Option<Str<'a>> {
        if let Some(Ok(str)) = self.peek(0).map(Str::try_from) {
            self.consume();
            return Some(str);
        }

        self.cursor = Cursor::new_at(self.source, self.pos());

        // all peeked tokens will be discared as what they were originally parsed,
        // and a single string token will be pushed instead.
        self.incoming_tokens = VecDeque::new();

        with_context!(self.diagnostics; ContextKind::ParsingUndelimitedString => {
            let mut token_diagnostics = Vec::new();

            let start_pos = self.cursor.pos();

            let string = self.parse_string_content(&mut token_diagnostics, |this| {
                let peek = this.cursor.peek();
                peek.is_none()
                    || peek.unwrap().is_newline()
                    || this.cursor.matches(";;")
            }, false);

            let end_pos = self.cursor.pos();

            Some(Str {
                string,
                span: Span::new(start_pos, end_pos),
            })
        })
    }

    pub fn skip_until(&mut self, kind: TokenKind) {
        while let Some(token) = self.peek(0) {
            if token.kind() == kind {
                return;
            }
            self.consume();
        }
    }

    pub fn skip_to_next_line(&mut self) {
        while let Some(token) = self.peek(0) {
            self.consume();
            if token.kind() == TokenKind::Newline {
                return;
            }
        }
    }

    pub fn consume_empty_lines(&mut self) {
        while let Some(token) = self.peek(0) {
            if token.kind() != TokenKind::Newline {
                return;
            }
            self.consume();
        }
    }

    fn lex(&mut self, token_diagnostics: &mut Vec<Diagnostic<'a>>) -> Option<Token<'a>> {
        self.cursor.skip_whitespace();

        let Some(c) = self.cursor.peek() else {
            return None;
        };

        if let Some(token) = self.try_lex_identifier(token_diagnostics) {
            return Some(token);
        }

        if let Some(token) = self.try_lex_delimited_string(token_diagnostics) {
            return Some(token);
        }

        let pos = self.cursor.pos();
        self.cursor.consume();

        let token = match c {
            '[' => Some(Token::create_left_bracket(Span::at(pos, c))),
            ']' => Some(Token::create_right_bracket(Span::at(pos, c))),
            '=' => Some(Token::create_equal(Span::at(pos, c))),

            '\n' => Some(Token::create_newline(Span::at(pos, c))),
            '\r' if self.cursor.peek() == Some('\n') => {
                self.cursor.consume();
                Some(Token::create_newline(Span::new(pos, self.cursor.pos())))
            }

            ';' if self.cursor.peek() == Some(';') => {
                self.cursor.consume();
                self.cursor.consume_while(|c| !c.is_newline());

                self.lex(token_diagnostics)
            }

            ';' if self.cursor.try_parse("--") => {
                loop {
                    if self.cursor.peek().is_none() {
                        self.diagnostics.borrow().add_to(
                            token_diagnostics,
                            Error::UnclosedMultilineComment,
                            Span::new(pos, self.cursor.pos()),
                        );
                        return None;
                    };

                    if self.cursor.try_parse("--;") {
                        break;
                    }

                    self.cursor.consume();

                    // if c == '*' && self.cursor.peek() == Some(';') {
                    //     self.cursor.consume();
                    //     break;
                    // }
                }

                self.lex(token_diagnostics)
            }

            _ => Some(Token::create_invalid_character(c, Span::at(pos, c))),
        };

        token
    }

    fn try_lex_identifier(&mut self, _: &mut Vec<Diagnostic<'a>>) -> Option<Token<'a>> {
        let Some(c) = self.cursor.peek() else {
            return None;
        };

        if !char::is_identifier_head(c) {
            return None;
        }

        let start_pos = self.cursor.pos();

        self.cursor.consume();
        self.cursor.consume_while(char::is_identifier_tail);

        let span = Span::new(start_pos, self.cursor.pos());

        Some(Token::create_identifier(span.slice(self.source), span))
    }

    fn try_lex_delimited_string(
        &mut self,
        token_diagnostics: &mut Vec<Diagnostic<'a>>,
    ) -> Option<Token<'a>> {
        let token_start_pos = self.cursor.pos();

        if !self.cursor.try_parse("''") {
            return None;
        }

        with_context!(self.diagnostics; ContextKind::ParsingDelimitedString => {
            let string = self.parse_string_content(token_diagnostics, |this| this.cursor.try_parse("''"), true);

            Some(Token::create_string(
                string,
                Span::new(token_start_pos, self.cursor.pos()),
            ))
        })
    }

    fn parse_string_content<P: FnMut(&mut Self) -> bool>(
        &mut self,
        token_diagnostics: &mut Vec<Diagnostic<'a>>,
        mut try_parse_end: P,
        ignore_multiline_comments: bool,
    ) -> Option<UnparsedStringExpression<'a>> {
        let mut string_tokens_couples = Vec::new();

        let mut current_string: Option<String> = None;
        let mut slice_start = self.cursor.pos();

        loop {
            // put it first, so it can accept EOF as end of string.
            let slice_end = self.cursor.pos();
            if try_parse_end(self) {
                let slice = Span::new(slice_start, slice_end).slice(self.source);

                if let Some(current_string) = &mut current_string {
                    current_string.push_str(slice);
                }

                let slice = current_string
                    .take()
                    .map(Cow::from)
                    .unwrap_or_else(|| Cow::from(slice));

                return Some(UnparsedStringExpression(string_tokens_couples, slice));
            }

            let prev_pos = self.cursor.pos();
            let Some(c) = self.cursor.next() else { break };

            match c {
                ';' if !ignore_multiline_comments && self.cursor.try_parse("--") => {
                    if current_string.is_none() {
                        current_string = Some(String::new());
                    }

                    let current_string = current_string.as_mut().unwrap();

                    current_string.push_str(Span::new(slice_start, prev_pos).slice(self.source));

                    loop {
                        if self.cursor.peek().is_none() {
                            self.diagnostics.borrow().add_to(
                                token_diagnostics,
                                Error::UnclosedMultilineComment,
                                Span::new(prev_pos, self.cursor.pos()),
                            );
                            return None;
                        };

                        if self.cursor.try_parse("--;") {
                            break;
                        }

                        self.cursor.consume();
                    }

                    slice_start = self.cursor.pos();
                }
                '\\' => match self.cursor.next() {
                    None => {
                        self.diagnostics.borrow().add_to(
                            token_diagnostics,
                            Error::EOF,
                            Span::empty(self.cursor.pos()),
                        );

                        return None;
                    }
                    Some(c) => {
                        let replacement = match c {
                            'n' => '\n',
                            't' => '\t',
                            'r' => '\r',
                            'u' => {
                                with_context!(self.diagnostics; ContextKind::ParsingUnicodeEscapeSequence => {
                                    with_context!(self.diagnostics; ContextKind::ExpectingCharacter('{') => {
                                        match self.cursor.peek() {
                                            Some('{') => Some(()),
                                            c => {
                                                let (diagnostic, span) = if let Some(c) = c {
                                                    (Error::UnexpectedCharacter(c), Span::at(self.cursor.pos(), c))
                                                } else {
                                                    (Error::EOF, Span::empty(self.cursor.pos()))
                                                };

                                                self.diagnostics.borrow().add_to(token_diagnostics, diagnostic, span);

                                                None
                                            }
                                        }
                                    })?;
                                    self.cursor.consume();

                                    let hex_start = self.cursor.pos();

                                    let mut count = 0;

                                    let hex_end = with_context!(self.diagnostics; ContextKind::Expecting("hex digit or character '}'") => loop {
                                        let Some(c) = self.cursor.peek() else {
                                            self.diagnostics.borrow().add_to(token_diagnostics, Error::EOF, Span::empty(self.cursor.pos()));
                                            return None;
                                        };

                                        if c == '}' {
                                            let hex_end = self.cursor.pos();

                                            self.cursor.consume();

                                            break Some(hex_end)
                                        }

                                        if count >= 6 {
                                            self.diagnostics.borrow_mut().replace_context(ContextKind::ExpectingCharacter('}'));

                                            self.diagnostics.borrow().add_to(token_diagnostics, Error::UnicodeEscapeSequenceTooManyDigits, Span::new(hex_start, self.cursor.pos().advance(c)));
                                            return None;
                                        }

                                        self.cursor.consume();
                                        count += 1;
                                    })?;

                                    let hex_span = Span::new(hex_start, hex_end);
                                    let hex_slice = hex_span.slice(self.source);

                                    if hex_start == hex_end {
                                        self
                                            .diagnostics
                                            .borrow()
                                            .add_to(token_diagnostics, Error::EmptyValue, Span::empty(hex_end));
                                        return None;
                                    }

                                    let Ok(value) = u32::from_str_radix(hex_slice, 16) else {
                                        self.diagnostics.borrow().add_to(token_diagnostics,
                                                Error::InvalidHexNumber(hex_slice),
                                                hex_span,
                                            );
                                        return None;
                                    };

                                    let Some(c) = char::from_u32(value) else {
                                        self.diagnostics.borrow().add_to(token_diagnostics,
                                                Error::InvalidUnicode(hex_slice),
                                                hex_span,
                                            );
                                        return None;
                                    };

                                    Some(c)
                                })?
                            }
                            c => c,
                        };

                        if current_string.is_none() {
                            current_string = Some(String::new());
                        }

                        let current_string = current_string.as_mut().unwrap();

                        current_string
                            .push_str(Span::new(slice_start, prev_pos).slice(self.source));
                        current_string.push(replacement);

                        slice_start = self.cursor.pos();
                    }
                },
                '[' => {
                    let slice = Span::new(slice_start, prev_pos).slice(self.source);

                    if let Some(current_string) = &mut current_string {
                        current_string.push_str(slice);
                    }

                    let slice = current_string
                        .take()
                        .map(Cow::from)
                        .unwrap_or_else(|| Cow::from(slice));

                    let mut tokens = Vec::new();
                    loop {
                        let token = self.lex(token_diagnostics);

                        let Some(token) = token else {
                            self.diagnostics.borrow().add_to(
                                token_diagnostics,
                                Error::EOF,
                                Span::empty(self.cursor.pos()),
                            );

                            return None;
                        };

                        if let Token::RightBracket(_) = token {
                            break;
                        }

                        tokens.push(token);
                    }

                    slice_start = self.cursor.pos();

                    string_tokens_couples.push((slice, tokens));
                }
                _ => {}
            }
        }

        self.diagnostics.borrow().add_to(
            token_diagnostics,
            Error::EOF,
            Span::empty(self.cursor.pos()),
        );

        None
    }
}

impl<'a> Iterator for Lexer<'a> {
    type Item = Token<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        let Some(token) = self.peek(0) else {
            return None;
        };

        self.consume();

        Some(token)
    }
}

impl<'a> Drop for Lexer<'a> {
    fn drop(&mut self) {
        let mut diagnostics = self.diagnostics.borrow_mut();
        for diagnostic in std::mem::take(&mut self.eof_diagnostics) {
            diagnostics.add_diagnostic(diagnostic);
        }
    }
}

pub trait Expectable<'a>: Iterator<Item = Token<'a>> {
    fn peek(&mut self, at: usize) -> Option<Token<'a>>;
    fn diagnostics(&self) -> &Rc<RefCell<DiagnosticList<'a>>>;
    fn pos(&self) -> Pos;

    fn try_parse<T: TokenValue<'a>>(&mut self) -> Option<T> {
        with_try_context!(self.diagnostics(); ContextKind::ExpectingToken(T::kind()) => {
            let Some(token) = self.peek(0) else {
                let pos = self.pos();
                self.diagnostics().borrow_mut().add(Error::EOF, Span::empty(pos));
                return None;
            };

            match token.clone().try_into() {
                Ok(value) => {
                    self.next();
                    Some(value)
                },
                Err(_) => {
                    let span = token.span();
                    self.diagnostics().borrow_mut().add(Error::UnexpectedToken(token), span);
                    None
                }
            }
        })
    }

    fn expect<T: TokenValue<'a>>(&mut self) -> Option<T> {
        unwrap_try!(self.diagnostics() => self.try_parse())
    }

    fn try_parse_or_eof<T: TokenValue<'a>>(&mut self) -> Option<Option<T>> {
        with_try!(self.diagnostics() => {
            if self.peek(0).is_none() {
                return Some(None);
            }

            self.expect().map(Some)
        })
    }

    fn expect_or_eof<T: TokenValue<'a>>(&mut self) -> Option<Option<T>> {
        unwrap_try!(self.diagnostics() => self.try_parse_or_eof())
    }

    fn try_parse_keyword(&mut self, keyword: Keyword) -> Option<Identifier<'a>> {
        with_try_context!(self.diagnostics(); ContextKind::ExpectingKeyword(keyword) => {
            let Some(token) = self.peek(0) else {
                let pos = self.pos();
                self.diagnostics().borrow_mut().add(Error::EOF, Span::empty(pos));
                return None;
            };

            match Identifier::try_from(token.clone()) {
                Ok(value) => {
                    if value.text == keyword.as_str() {
                        self.next();
                        Some(value)
                    } else {
                        None
                    }
                },
                Err(_) => {
                    let span = token.span();
                    self.diagnostics().borrow_mut().add(Error::UnexpectedToken(token), span);
                    None
                }
            }
        })
    }

    fn expect_keyword(&mut self, keyword: Keyword) -> Option<Identifier<'a>> {
        unwrap_try!(self.diagnostics() => self.try_parse_keyword(keyword))
    }

    fn try_parse_eof(&mut self) -> Option<()> {
        with_try_context!(self.diagnostics(); ContextKind::ExpectingEOF => {
            let Some(token) = self.peek(0) else {
                return Some(())
            };

            let span = token.span();
            self.diagnostics().borrow_mut().add(Error::UnexpectedToken(token), span);

            None
        })
    }

    fn expect_eof(&mut self) -> Option<()> {
        unwrap_try!(self.diagnostics() => self.try_parse_eof())
    }
}

pub struct TokenIter<'a, 'b> {
    list: &'b [Token<'a>],
    index: usize,
    diagnostics: Rc<RefCell<DiagnosticList<'a>>>,
}

impl<'a, 'b> TokenIter<'a, 'b> {
    pub fn new(list: &'b [Token<'a>], diagnostics: Rc<RefCell<DiagnosticList<'a>>>) -> Self {
        if list.is_empty() {
            panic!("Cannot construct a `TokenIter` with an empty list.");
        }

        Self {
            list,
            index: 0,
            diagnostics,
        }
    }
}

impl<'a, 'b> Iterator for TokenIter<'a, 'b> {
    type Item = Token<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        self.index += 1;
        self.list.get(self.index).cloned()
    }
}

impl<'a, 'b> Expectable<'a> for TokenIter<'a, 'b> {
    fn peek(&mut self, at: usize) -> Option<Token<'a>> {
        self.list.get(self.index + at).cloned()
    }

    fn diagnostics(&self) -> &Rc<RefCell<DiagnosticList<'a>>> {
        &self.diagnostics
    }

    fn pos(&self) -> Pos {
        self.list
            .get(self.index + 1)
            .map(|token| token.span().start)
            .unwrap_or(self.list.last().unwrap().span().end)
    }
}

impl<'a> Expectable<'a> for Lexer<'a> {
    fn peek(&mut self, at: usize) -> Option<Token<'a>> {
        self.peek(at)
    }

    fn diagnostics(&self) -> &Rc<RefCell<DiagnosticList<'a>>> {
        &self.diagnostics
    }

    fn pos(&self) -> Pos {
        self.pos()
    }
}
