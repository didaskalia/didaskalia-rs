use std::{borrow::Cow, cell::RefCell, collections::HashMap, ffi::OsStr, fs, rc::Rc};

use crate::{
    diagnostics::DiagnosticList,
    entry_collection::{EntryCollection, EntryPath},
    parser::Parser,
};

#[derive(Default)]
pub struct DocumentBuilder {
    content: HashMap<String, EntryCollection>,
    staged_files: Vec<String>,
}

impl DocumentBuilder {
    pub fn new() -> DocumentBuilder {
        Self {
            content: HashMap::new(),
            staged_files: Vec::new(),
        }
    }

    pub fn add_file(mut self, path: &str) -> Self {
        self.staged_files.push(path.to_string());
        self
    }

    pub fn add_dir(mut self, path: &str) -> Self {
        let Ok(dir) = fs::read_dir(path) else {
            eprintln!("ERROR: could not read dir '{path}'!");
            return self;
        };

        for entry in dir {
            let path = match entry {
                Ok(entry) => entry.path(),
                Err(e) => {
                    eprintln!("ERROR: could not unwrap entry! ({})", e);
                    continue;
                }
            };

            let Some("dsk") = path.extension().and_then(OsStr::to_str) else {
                continue;
            };

            let Some(path) = path.to_str() else {
                eprintln!("ERROR: failed to read entry!");
                continue;
            };

            self = self.add_file(path);
        }

        self
    }

    // TODO: handle errors, return Err(...) if any
    pub fn build(mut self) -> Document {
        for path in self.staged_files {
            let source = match fs::read_to_string(path.as_str()) {
                Ok(source) => source,
                Err(e) => {
                    eprintln!("ERROR: could not read file '{path}': {e}");
                    continue;
                }
            };
            let source = source.as_str();

            let diagnostics = Rc::new(RefCell::new(DiagnosticList::new()));
            Parser::new(source, diagnostics.clone()).parse(&mut self.content);

            let diagnostics = Rc::into_inner(diagnostics).unwrap().into_inner();

            diagnostics.print(source);
        }

        Document {
            content: self.content,
        }
    }
}

pub struct Document {
    content: HashMap<String, EntryCollection>,
}

impl Document {
    pub fn get(&self, lang: &str, entry_path: &EntryPath) -> Option<Cow<'_, str>> {
        let string_expression = self.content.get(lang)?.get_entry_with_path(entry_path)?;

        if string_expression.0.is_empty() {
            return Some(Cow::from(&string_expression.1));
        }

        let mut ret_string = String::new();

        for (s, entry_path) in &string_expression.0 {
            ret_string.push_str(s);
            ret_string.push_str(&self.get(lang, entry_path)?);
        }

        ret_string.push_str(&string_expression.1);

        Some(Cow::from(ret_string))
    }
}
