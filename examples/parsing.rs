use std::borrow::Cow;

use didaskalia::DocumentBuilder;

#[rustfmt::skip]
fn main() {
    let doc = DocumentBuilder::new().add_dir("samples").build();

    println!();
    println!("{}", doc.get("english", &"welcome".try_into().unwrap()).unwrap_or(Cow::from("<???>")));
    println!("{}", doc.get("english", &"about00".try_into().unwrap()).unwrap_or(Cow::from("<???>")));
    println!("{}", doc.get("english", &"goodbye".try_into().unwrap()).unwrap_or(Cow::from("<???>")));
    println!();
    println!("{}", doc.get("french", &"welcome".try_into().unwrap()).unwrap_or(Cow::from("<???>")));
    println!("{}", doc.get("french", &"about00".try_into().unwrap()).unwrap_or(Cow::from("<???>")));
    println!("{}", doc.get("french", &"goodbye".try_into().unwrap()).unwrap_or(Cow::from("<???>")));
}
